﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CompanyStructure
{
    public static class Util
    {

        public static int GetAge(DateTime birthDate, DateTime dateToCalc)
        {
            int result = dateToCalc.Year - birthDate.Year;

            if (birthDate.Month > dateToCalc.Month || birthDate.Month == dateToCalc.Month && birthDate.Day > dateToCalc.Day)
                result--;

            return result;
        }

        public static int GetAge(DateTime birthDate) => GetAge(birthDate, DateTime.Now);

        public static string AgeToString(int age) => CntToString(age, "год", "года", "лет");

        public static string SymbolsCntToString(int symbolsCnt) => CntToString(symbolsCnt, "символ", "символа", "символов");


        public static string CntToString(int cnt, string nomSingForm, string genSingForm, string genPlurForm)
        {
            int rem = cnt % 100;

            if (rem >= 10 && rem <= 20)
                return $"{cnt} {genPlurForm}";
            else
            {
                rem %= 10;
                if (rem == 1)
                    return $"{cnt} {nomSingForm}";
                else if (rem >= 2 && rem <= 4)
                    return $"{cnt} {genSingForm}";
                else
                    return $"{cnt} {genPlurForm}";
            }
        }

    }
}
