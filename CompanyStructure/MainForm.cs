﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace CompanyStructure
{
    public partial class MainForm : Form
    {

        bool lazyLoadDeps;
        bool selectFromUpdate;
        bool cbSet;
        TreeNode curNode;
        TreeNode[] curLevel;
        int curNodeIndex;
        Department curDepartment;
        Department curDepartmentOrigin;
        Dictionary<string, bool> expanded = new Dictionary<string, bool>();
        Tuple<string, string>[] allDepsIdsAndNames;
        Timer timer = new Timer();
        QueryManager queryManager = new QueryManager();

        public MainForm()
        {
            InitializeComponent();
            RefreshInfo();
            timer.Tick += (o, e) => RefreshInfo(autoUpdate: true);
            cbActNotSave.SelectedIndex = 0;
            SetTimerInterval();
            timer.Start();
        }

        private void RefreshInfo(bool autoUpdate = false)
        {
            try
            {
                allDepsIdsAndNames = queryManager.GetAllDepsIdsAndNames();
                FillTreeView();
                UpdateCurNode();
                ChooseCurDepartment(curNode, autoUpdate);
            }
            catch (Exception ex)
            {
                MessageBox.Show($"Возникла ошибка: {ex.Message} \r\nПовторите попытку позже (кнопка \"Обновить информацию из БД\")");
            }
        }

        private void ChooseCurDepartment(TreeNode node = null, bool autoUpdate = false)
        {
            if (treeView.Nodes.Count == 0)
            {
                MessageBox.Show("Не загружена информация об отделах. Повторите попытку позже");
                return;
            }

            curNode = node ?? treeView.Nodes[0];


            bool withEmployees = true;

            try
            {
                curDepartmentOrigin = queryManager.GetDepartment(Guid.Parse(curNode.Name + ""), withEmployees);
                if (curDepartmentOrigin == null)
                {
                    curDepartmentOrigin = queryManager.GetRoot(withEmployees);
                    curNode = treeView.Nodes[0];
                }
            }
            catch
            {
                MessageBox.Show("В базе данных не найдены ни выбранный, ни корневой отдел. Обратитесь к администратору БД или повторите попытку позже");
                return;
            }

            selectFromUpdate = true;
            treeView.SelectedNode = curNode;
            selectFromUpdate = false;

            if (autoUpdate)
                return;

            curDepartment = curDepartmentOrigin.MakeCopy();
            
            treeView.Focus();

            curLevel = curNode.Parent?.Nodes?.Cast<TreeNode>().ToArray() ?? new TreeNode[] { curNode };
            curNodeIndex = Array.IndexOf(curLevel, curNode);
            RefreshNavigationButtonsEnabled();

            UpdateDepartmentLayout();
        }

        private void UpdateDepartmentLayout()
        {
            tbCurDepName.Text = curDepartment.Name;
            tbCurDepCode.Text = curDepartment.Code;

            dataGridView1.Rows.Clear();
            List<DataGridViewRow> rows = new List<DataGridViewRow>();
            foreach (Empoyee emp in curDepartment.Empoyee)
            {
                rows.Add(new DataGridViewRow());
                string fullName = $"{emp.FirstName} {emp.SurName}";
                rows[rows.Count - 1].CreateCells(dataGridView1, emp.ID, fullName, emp.Position, emp.GetAge(DateTime.Now));
            }
            dataGridView1.Rows.AddRange(rows.ToArray());
            btnRemoveEmps.Enabled = false;

            cbSet = false;
            if (curNode == treeView.Nodes[0])
            {
                cbCurDepParentDep.DataSource = null;
                cbCurDepParentDep.Enabled = false;
            }
            else
            {
                cbCurDepParentDep.DataSource = allDepsIdsAndNames.Where(t => t.Item1 != curDepartment.ID + "").ToList();
                cbCurDepParentDep.SelectedItem = allDepsIdsAndNames.FirstOrDefault(t => t.Item1 == curDepartment.ParentDepartmentID + "");
                cbCurDepParentDep.Enabled = true;
            }
            cbSet = true;
            RefreshSaveAndUndoButtonsEnabled();
        }


        private void RemoveEmployee(decimal id)
        {
            queryManager.RemoveEmployee(id);
            ChooseCurDepartment(curNode);
        }

        private void RemoveEmployees(decimal[] ids)
        {
            queryManager.RemoveEmployees(ids);
            ChooseCurDepartment(curNode);
        }

        public TreeNode FindNode(string name) => FindNode(treeView.Nodes[0], name);

        public TreeNode FindNode(TreeNode curRoot, string name)
        {
            if (curRoot.Name == name)
            {
                return curRoot;
            }
            else
            {
                foreach (TreeNode child in curRoot.Nodes)
                {
                    var result = FindNode(child, name);
                    if (result != null)
                        return result;
                }
                return null;
            }
        }

        private void FillTreeView()
        {
            if (lazyLoadDeps)
                FillTreeViewLazy();
            else
                FillTreeViewFull();
            RestoreExpand();
        }

        private void RestoreExpand()
        {
            if (treeView.Nodes.Count > 0)
                RestoreExpand(treeView.Nodes[0]);
        }

        private void RestoreExpand(TreeNode node)
        {
            if (expanded.ContainsKey(node.Name) && expanded[node.Name])
                node.Expand();
            else
                node.Collapse();
            foreach (TreeNode childNode in node.Nodes)
                RestoreExpand(childNode);
        }

        private void FillTreeViewLazy()
        {
            treeView.Nodes.Clear();

            DepartmentBriefInfo root;

            try
            {
                root = queryManager.GetRootInfo();
            }
            catch
            {
                MessageBox.Show("В базе данных не найден корневой отдел. Обратитесь к администратору БД или повторите попытку позже");
                return;
            }

            treeView.Nodes.Add(root.ID + "", root.Name);

            DepartmentBriefInfo[] firstLevelDeps;

            try
            {
                firstLevelDeps = queryManager.GetChildDepsInfos(root.ID);
            }
            catch
            {
                MessageBox.Show("Ошибка при чтении отделов первого уровня. Обратитесь к администратору БД или повторите попытку позже");
                return;
            }

            foreach(var d in firstLevelDeps)
            {
                treeView.Nodes[0].Nodes.Add(d.ID + "", d.Name);
                if (d.HasChildDeps)
                    treeView.Nodes[0].Nodes[d.ID + ""].Nodes.Add("<blank>");
            }

        }

        private void FillTreeViewFull()
        {
            treeView.Nodes.Clear();

            Department[] allDeps;

            try
            {
                allDeps = queryManager.GetAllDeps();
            }
            catch
            {
                MessageBox.Show("Ошибка при загрузке списка отделов. Обратитесь к администратору БД или повторите попытку позже");
                return;
            }

            var root = allDeps.FirstOrDefault(d => d.ParentDepartment == null);

            treeView.Nodes.Add(root.ID + "", root.Name);

            foreach (Department d in root.ChildDepartments)
                HangSubTree(treeView.Nodes[0], d);

        }

        private void HangSubTree(TreeNode parent, Department child)
        {
            parent.Nodes.Add(child.ID + "", child.Name);
            foreach (var d in child.ChildDepartments)
            {
                HangSubTree(parent.Nodes[child.ID + ""], d);
            }
        }



        private void SetTimerInterval() => timer.Interval = (int)TimeSpan.FromMinutes((double)nudFreq.Value).TotalMilliseconds;



        #region Navigation by buttons and hotkeys

        private void btnUp_Click(object sender, EventArgs e) => GoUp();

        private void btnDown_Click(object sender, EventArgs e) => GoDown();

        private void btnLeft_Click(object sender, EventArgs e) => GoBack();

        private void btnRight_Click(object sender, EventArgs e) => GoForward();

        private void GoUp()
        {
            btnUp.Focus();
            if (!CanGoUp())
                return;
            ChooseCurDepartment(curNode.Parent);
            curNode.Collapse();
        }

        private void GoDown()
        {
            btnDown.Focus();
            if (!CanGoDown())
                return;
            ChooseCurDepartment(curNode.Nodes[0]);
        }

        private void GoForward()
        {
            btnRight.Focus();
            if (!CanGoForward())
                return;
            ChooseCurDepartment(curLevel[curNodeIndex + 1]);
        }

        private void GoBack()
        {
            btnLeft.Focus();
            if (!CanGoBack())
                return;
            ChooseCurDepartment(curLevel[curNodeIndex - 1]);
        }


        private void RefreshNavigationButtonsEnabled()
        {
            btnUp.Enabled = CanGoUp();
            btnDown.Enabled = CanGoDown();
            btnRight.Enabled = CanGoForward();
            btnLeft.Enabled = CanGoBack();
        }

        private bool CanGoUp() => curNode?.Parent != null;
        private bool CanGoDown() => curNode?.Nodes?.Count > 0;
        private bool CanGoForward() => curNodeIndex < curLevel?.Length - 1;
        private bool CanGoBack() => curNodeIndex > 0;

        private void FormKeyDown(object sender, KeyEventArgs e)
        {
            if (e.Alt)
            {
                e.SuppressKeyPress = true;
                switch (e.KeyCode)
                {
                    case Keys.Up:
                    case Keys.W:
                        GoUp();
                        break;
                    case Keys.Down:
                    case Keys.S:
                        GoDown();
                        break;
                    case Keys.Right:
                    case Keys.D:
                        GoForward();
                        break;
                    case Keys.Left:
                    case Keys.A:
                        GoBack();
                        break;
                }
            }
        }

        #endregion

        private void cbAutoUpdate_CheckedChanged(object sender, EventArgs e)
        {
            if (cbAutoUpdate.Checked)
                timer.Start();
            else
                timer.Stop();
        }

        private void nudFreq_ValueChanged(object sender, EventArgs e)
        {
            SetTimerInterval();
        }



        private Department FindDep(string id)
        {
            using (var db = new CompanyStructureDbContext())
            {
                return db.Departments.FirstOrDefault(d => d.ID + "" == id);
            }
        }

        private void treeView_BeforeExpand(object sender, TreeViewCancelEventArgs e)
        {
            //ChooseCurDepartment(e.Node);
            expanded[e.Node.Name] = true;

            if (e.Node == treeView.Nodes[0] || !lazyLoadDeps)
                return;

            e.Node.Nodes.Clear();

            TreeNode[] childNodes;

            try
            {
                childNodes = queryManager.GetChildDepsInfos(Guid.Parse(e.Node.Name))
                    .Select(d => new TreeNode { Name = d.ID + "", Text = d.Name })
                    .ToArray();
            }
            catch
            {
                MessageBox.Show($"Ошибка при чтении дочерних отделов отдела \"{e.Node.Text}\". Обратитесь к администратору БД или повторите попытку позже");
                return;
            }

            e.Node.Nodes.AddRange(childNodes);
        }

        private void treeView_BeforeCollapse(object sender, TreeViewCancelEventArgs e)
        {
            expanded[e.Node.Name] = false;
            if (e.Node == treeView.Nodes[0] || !lazyLoadDeps)
                return;
            e.Node.Nodes.Clear();
            e.Node.Nodes.Add("<blank>");
        }
        

        private void cbLazyLoad_CheckedChanged(object sender, EventArgs e)
        {
            lazyLoadDeps = cbLazyLoad.Checked;
        }

        private void treeView_AfterSelect(object sender, TreeViewEventArgs e)
        {
            if (selectFromUpdate)
                return;
            ChooseCurDepartment(e.Node);
            btnRemoveCurDep.Enabled = treeView.SelectedNode != treeView.Nodes[0];
        }

        private void SetRemoveChoosenEnabled()
        {
            int count = dataGridView1.Rows.Cast<DataGridViewRow>().Count(row => Convert.ToBoolean(row.Cells[6].Value) == true);
            btnRemoveEmps.Enabled = count > 0;
        }

        private void dataGridView1_CellContentClick(object sender, DataGridViewCellEventArgs e)
        {
            switch (e.ColumnIndex)
            {
                case 4:
                    ShowEmployeeInfo((decimal)dataGridView1.Rows[e.RowIndex].Cells[0].Value);
                    break;
                case 5:
                    if (cbAskDel.Checked)
                        if (MessageBox.Show("Вы уверены? Отменить это действие невозможно", "Удаление сотрудника",
                             MessageBoxButtons.OKCancel) != DialogResult.OK)
                            return;
                    RemoveEmployee((decimal)dataGridView1.Rows[e.RowIndex].Cells[0].Value);
                    break;
                case 6:
                    dataGridView1.CommitEdit(DataGridViewDataErrorContexts.Commit);
                    break;
            }

        }

        private void ShowEmployeeInfo(decimal id)
        {
            var emp = queryManager.GetEmpoyee(id);
            var form = new EmployeeEditForm(emp, allDepsIdsAndNames, queryManager, isNew: false);
            if (cbAutoUpdate.Checked)
            {
                timer.Stop();
                form.FormClosed += (_, __) => timer.Start();
            }
            form.FormClosed += (_, __) =>
            {
                queryManager.Save(curDepartment);
                RefreshInfo();
            };
            form.ShowDialog();
        }
        
        private void UpdateCurNode()
        {
            if (curNode == null)
                return;
            curNode = FindNode(curNode.Name) ?? (treeView.Nodes.Count > 0 ? treeView.Nodes[0] : null);
        }

        private void dataGridView1_CellValueChanged(object sender, DataGridViewCellEventArgs e)
        {
            switch (e.ColumnIndex)
            {
                case 6:
                    SetRemoveChoosenEnabled();
                    break;
            }
        }

        private void btnRemoveEmps_Click(object sender, EventArgs e)
        {
            if (cbAskDel.Checked)
                if (MessageBox.Show("Вы уверены? Отменить это действие невозможно", "Удаление сотрудников",
                     MessageBoxButtons.OKCancel) != DialogResult.OK)
                    return;

            decimal[] ids = dataGridView1.Rows.Cast<DataGridViewRow>().
                Where(row => Convert.ToBoolean(row.Cells[6].Value) == true).
                Select(row => Convert.ToDecimal(row.Cells[0].Value)).ToArray();

            RemoveEmployees(ids);
        }

        private void cbCurDepParentDep_Format(object sender, ListControlConvertEventArgs e)
        {
            e.Value = ((Tuple<string, string>)e.ListItem).Item2;
        }
        
        private void cbCurDepParentDep_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (!cbSet)
                return;
            var parentDepInfo = cbCurDepParentDep.SelectedItem as Tuple<string, string>;
            curDepartment.ParentDepartmentID = Guid.Parse(parentDepInfo.Item1);
            RefreshSaveAndUndoButtonsEnabled();
        }

        private void btnSaveDepartmentInfoChanges_Click(object sender, EventArgs e)
        {
            queryManager.Save(curDepartment);
            curNode.Text = curDepartment.Name;
            curDepartmentOrigin = curDepartment.MakeCopy();
            RefreshSaveAndUndoButtonsEnabled();
        }

        private void RefreshSaveAndUndoButtonsEnabled()
        {
            btnUndoDepartmentInfoChanges.Enabled = !curDepartment.Equals(curDepartmentOrigin);
            btnSaveDepartmentInfoChanges.Enabled = !curDepartment.Equals(curDepartmentOrigin) 
                && lblErrCode.Text == "" && lblErrName.Text == "";
        }

        private void tbCurDepName_TextChanged(object sender, EventArgs e)
        {
            string name = tbCurDepName.Text.Trim();
            curDepartment.Name = name;
            if (name.Length > 50)
                lblErrName.Text = "Максимально допустимая длина наименования - 50";
            else if (name == "")
                lblErrName.Text = "Поле \"Наименование\" не может быть пустым";
            else
                lblErrName.Text = "";
            RefreshSaveAndUndoButtonsEnabled();
        }

        private void tbCurDepCode_TextChanged(object sender, EventArgs e)
        {
            string code = tbCurDepCode.Text.Trim();
            curDepartment.Code = code == "" ? null : code;
            if (code.Length > 10)
                lblErrCode.Text = "Максимально допустимая длина мнемокода - 10";
            else
                lblErrCode.Text = "";
            RefreshSaveAndUndoButtonsEnabled();
        }

        private void btnUndoDepartmentInfoChanges_Click(object sender, EventArgs e)
        {
            curDepartment = curDepartmentOrigin.MakeCopy();
            UpdateDepartmentLayout();
        }

        private void treeView_BeforeSelect(object sender, TreeViewCancelEventArgs e)
        {
            if (selectFromUpdate)
                return;
            if (curDepartment == null || curDepartmentOrigin == null ||
                curDepartment.Equals(curDepartmentOrigin))
                return;


            bool save = false;
            bool stop = false;

            switch (cbActNotSave.SelectedIndex)
            {
                case 0:
                    var dialogRes = MessageBox.Show("Сохранить изменения в текущем отделе?", "Смена отдела", MessageBoxButtons.YesNoCancel);
                    switch (dialogRes)
                    {
                        case DialogResult.Yes:
                            save = true;
                            break;
                        case DialogResult.Cancel:
                            stop = true;
                            break;
                    }
                    break;
                case 1:
                    save = true;
                    break;
            }

            if (save)
            {
                queryManager.Save(curDepartment);
                curNode.Text = curDepartment.Name;
            }
            if (stop)
                e.Cancel = true;
        }

        private void btnUpdateInfo_Click(object sender, EventArgs e) => RefreshInfo(autoUpdate: true);

        private void btnAddEmployee_Click(object sender, EventArgs e)
        {
            var form = new EmployeeEditForm(
                new Empoyee
                {
                    DateOfBirth = new DateTime(1985, 1, 1),
                    DepartmentID = curDepartment.ID
                }, allDepsIdsAndNames, queryManager, isNew: true);
            if (cbAutoUpdate.Checked)
            {
                timer.Stop();
                form.FormClosed += (_, __) => timer.Start();
            }
            form.FormClosed += (_, __) =>
            {
                queryManager.Save(curDepartment);
                RefreshInfo();
            };
            form.ShowDialog();

        }

        private void btnAddNewDep_Click(object sender, EventArgs e)
        {
            bool save = false;

            if (curDepartment != null && curDepartmentOrigin != null &&
                !curDepartment.Equals(curDepartmentOrigin))
            {
                
                bool cancel = false;

                switch (cbActNotSave.SelectedIndex)
                {
                    case 0:
                        var dialogRes = MessageBox.Show("Сохранить изменения в текущем отделе?", "Добавление нового отдела", MessageBoxButtons.YesNoCancel);
                        switch (dialogRes)
                        {
                            case DialogResult.Yes:
                                save = true;
                                break;
                            case DialogResult.Cancel:
                                cancel = true;
                                break;
                        }
                        break;
                    case 1:
                        save = true;
                        break;
                }

                if (save)
                    queryManager.Save(curDepartment);
                if (cancel)
                    return;
            }

            var dep = new Department { ID = Guid.NewGuid(), Name = "<new department>", ParentDepartmentID = Guid.Parse(curNode.Name) };
            int attemtsCnt = 0;
            bool success = false;
            Exception exc = null;
            while (attemtsCnt < 5 && !success)
            {
                try
                {
                    queryManager.Insert(dep);
                    success = true;
                }
                catch (Exception ex)
                {
                    exc = ex;
                    attemtsCnt++;
                }
            }
            if (!success)
            {
                MessageBox.Show($"Не удалось добавить новый отдел. Повторите попытку позже. Текст ошибки:\r\n{exc?.Message}");
                return;
            }
            if (save)
                queryManager.Save(curDepartment);
            curNode = curNode.Nodes.Add(dep.ID + "", dep.Name);
            RefreshInfo();
        }

        private void btnRemoveCurDep_Click(object sender, EventArgs e)
        {
            if (cbAskDel.Checked && MessageBox.Show(
                $"Вы уверены, что хотите удалить отдел {curNode.Text}? Это действие нельзя будет отменить",
                "Удаление", MessageBoxButtons.OKCancel) != DialogResult.OK)
            {
                return;
            }


            queryManager.RemoveDepartment(Guid.Parse(curNode.Name));

            if (CanGoForward())
                GoForward();
            else if (CanGoBack())
                GoBack();
            else
                GoUp();
            RefreshInfo(autoUpdate: true);
        }

        private void MainForm_FormClosing(object sender, FormClosingEventArgs e)
        {
            if (curDepartment == null || curDepartmentOrigin == null ||
                curDepartment.Equals(curDepartmentOrigin))
                return;


            bool save = false;
            bool quit = true;

            switch (cbActNotSave.SelectedIndex)
            {
                case 0:
                    var dialogRes = MessageBox.Show("Сохранить изменения в текущем отделе?", "Выход", MessageBoxButtons.YesNoCancel);
                    switch (dialogRes)
                    {
                        case DialogResult.Yes:
                            save = true;
                            break;
                        case DialogResult.Cancel:
                            quit = false;
                            break;
                    }
                    break;
                case 1:
                    save = true;
                    break;
            }

            if (save)
                queryManager.Save(curDepartment);
            if (!quit)
                e.Cancel = true;
        }
        
    }
        
    
}
