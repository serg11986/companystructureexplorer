﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using static CompanyStructure.Util;

namespace CompanyStructure
{
    public partial class EmployeeEditForm : Form
    {
        Empoyee empoyee;
        Empoyee empoyeeOrigin;
        QueryManager queryManager;
        bool init = true;
        bool isNew;
        Label[] errLabels;

        public EmployeeEditForm(Empoyee emp, Tuple<string, string>[] allDepsIdsAndNames, QueryManager manager, bool isNew)
        {
            InitializeComponent();
            this.isNew = isNew;
            errLabels = new Label[]
            {
                lblDateError, lblErrDocNum,
                lblErrDocSer, lblErrName,
                lblErrPatr, lblErrPos, lblErrSurname
            };
            queryManager = manager;
            empoyeeOrigin = emp;
            empoyee = emp.MakeCopy();
            cbDep.DataSource = allDepsIdsAndNames;
            FillEmpInfo();
            CheckAll();
            init = false;
        }


        private void FillEmpInfo()
        {
            init = true;
            tbName.Text = empoyee.FirstName;
            tbPatr.Text = empoyee.Patronymic;
            tbSurname.Text = empoyee.SurName;
            tbPosition.Text = empoyee.Position;
            tbDocSer.Text = empoyee.DocSeries;
            tbDocNum.Text = empoyee.DocNumber;
            cbDep.SelectedItem = ((Tuple<string, string>[])cbDep.DataSource).FirstOrDefault(t => t.Item1 == empoyee.DepartmentID + "");
            dateTimePicker1.Value = empoyee.DateOfBirth;
            init = false;
        }

        private void CheckAndShowAge()
        {
            string check = CheckAge();
            if (check == "")
            {
                lblAge.Text = $"Возраст сотрудника: {AgeToString(GetAge(dateTimePicker1.Value))}";
                lblDateError.Text = "";
            }
            else
            {
                lblAge.Text = "";
                lblDateError.Text = check;
            }
        }

        private void dateTimePicker1_ValueChanged(object sender, EventArgs e)
        {
            CheckAndShowAge();
        }


        private string CheckAge()
        {
            if (dateTimePicker1.Value >= DateTime.Now)
            {
                return "Дата рождения должна быть в прошлом";
            }
            int age = GetAge(dateTimePicker1.Value);
            if (age < 16)
            {
                return $"Возраст сотрудника (полных лет): {age}. Минимальный допустимый возраст сотрудника - 16 лет";
            }
            if (age > 100)
            {
                return $"Возраст сотрудника (полных лет): {age}. Максимальный допустимый возраст сотрудника - 100 лет";
            }
            return "";
        }

        private void tbDocNum_KeyPress(object sender, KeyPressEventArgs e)
        {
            if (!char.IsControl(e.KeyChar) && !char.IsDigit(e.KeyChar))
            {
                e.Handled = true;
            }
        }

        private void cbDep_Format(object sender, ListControlConvertEventArgs e)
        {
            e.Value = ((Tuple<string, string>)e.ListItem).Item2;
        }

        private void CheckAll()
        {
            string text = tbName.Text.Trim();
            CheckTbText(text, lblErrName);
            text = tbPatr.Text.Trim();
            CheckTbText(text, lblErrPatr, isNullable: true);
            text = tbSurname.Text.Trim();
            CheckTbText(text, lblErrSurname);
            text = tbPosition.Text.Trim();
            CheckTbText(text, lblErrPos);
            text = tbDocSer.Text.Trim();
            CheckTbText(text, lblErrDocSer, minLength: 4, maxLength: 4, isNullable: true);
            text = tbDocNum.Text.Trim();
            CheckTbText(text, lblErrDocNum, minLength: 6, maxLength: 6, digitsOnly: true, isNullable: true);
        }

        private void CheckTbText(string text, Label lblErr, int minLength = 0, int maxLength = 50, bool digitsOnly = false, bool isNullable = false)
        {
            text = text.Trim();

            if (digitsOnly && text.Any(c => !char.IsDigit(c)))
                lblErr.Text = "Это поле должно содержать только цифры";
            else if (!isNullable && text.Length == 0)
                lblErr.Text = "Это поле обязательно для заполнения (не допускается пустая строка или строка из одних пробелов)";
            else if (minLength == maxLength && text.Length > 0 && text.Length != maxLength)
                lblErr.Text = $"Это должно содержать ровно {SymbolsCntToString(minLength)} (пробелы в начале и в конце игнорируются)";
            else if (text.Length > maxLength)
                lblErr.Text = $"Это поле не может содержать больше чем {SymbolsCntToString(minLength)}";
            else
                lblErr.Text = "";

            UpdateButtonsEnabled();
        }

        private void UpdateButtonsEnabled()
        {
            btnUndoChanges.Enabled = !empoyee.Equals(empoyeeOrigin);
            btnSaveChanges.Enabled = btnSaveQuit.Enabled = errLabels.All(l => l.Text == "") && btnUndoChanges.Enabled;
        }

        private void tbName_TextChanged(object sender, EventArgs e)
        {
            string text = tbName.Text.Trim();
            if (!init)
                empoyee.FirstName = text;
            CheckTbText(text, lblErrName);
        }

        private void tbPatr_TextChanged(object sender, EventArgs e)
        {
            string text = tbPatr.Text.Trim();
            if (!init)
                empoyee.Patronymic = string.IsNullOrEmpty(text) ? null : text;
            CheckTbText(text, lblErrPatr, isNullable: true);
        }

        private void tbSurname_TextChanged(object sender, EventArgs e)
        {
            string text = tbSurname.Text.Trim();
            if (!init)
                empoyee.SurName = text;
            CheckTbText(text, lblErrSurname);
        }

        private void cbDep_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (!init)
                empoyee.DepartmentID = Guid.Parse(((Tuple<string, string>)cbDep.SelectedItem).Item1);

            UpdateButtonsEnabled();

        }

        private void tbPosition_TextChanged(object sender, EventArgs e)
        {
            string text = tbPosition.Text.Trim();
            if (!init)
                empoyee.Position = text;
            CheckTbText(text, lblErrPos);
        }

        private void tbDocSer_TextChanged(object sender, EventArgs e)
        {
            string text = tbDocSer.Text.Trim();
            if (!init)
                empoyee.DocSeries = string.IsNullOrEmpty(text) ? null : text;
            CheckDocSer();
            CheckDocNum();
        }

        private void tbDocNum_TextChanged(object sender, EventArgs e)
        {
            string text = tbDocNum.Text.Trim();
            if (!init)
                empoyee.DocNumber = string.IsNullOrEmpty(text) ? null : text;
            CheckDocNum();
            CheckDocSer();
        }        
        

        private void CheckDocNum()
        {
            string text = tbDocNum.Text.Trim();
            CheckTbText(text, lblErrDocNum, minLength: 6, maxLength: 6, digitsOnly: true, isNullable: true);
            if (lblErrDocNum.Text == "" && text == "" && tbDocSer.Text.Trim() != "")
                lblErrDocNum.Text = "При вводе серии документа ввод номера становится обязательным";
        }

        private void CheckDocSer()
        {
            string text = tbDocSer.Text.Trim();
            CheckTbText(text, lblErrDocSer, minLength: 4, maxLength: 4, isNullable: true);
            if (lblErrDocSer.Text == "" && text == "" && tbDocNum.Text.Trim() != "")
                lblErrDocSer.Text = "При вводе номера документа ввод серии становится обязательным";
        }

        private void btnUndoChanges_Click(object sender, EventArgs e)
        {
            empoyee = empoyeeOrigin.MakeCopy();
            FillEmpInfo();
        }

        private void btnQuitWithoutSave_Click(object sender, EventArgs e)
        {
            Close();
        }

        private void btnSaveQuit_Click(object sender, EventArgs e)
        {
            if (Save())
                Close();
            else if (MessageBox.Show("Не удалось сохранить информацию в БД. Выйти без сохранения?",
                "Выход", MessageBoxButtons.OKCancel) == DialogResult.OK)
                Close();
        }

        private void btnSaveChanges_Click(object sender, EventArgs e)
        {
            Save();
        }

        private bool Save()
        {
            if (!empoyee.IsCorrect())
            {
                MessageBox.Show("Проверьте правильность заполнения всех полей");
                return false;
            }
            try
            {
                if (isNew)
                    queryManager.Insert(empoyee);
                else
                    queryManager.Save(empoyee);
                empoyeeOrigin = empoyee.MakeCopy();
            }
            catch (Exception ex)
            {
                MessageBox.Show($"Не удалось сохранить информацию в БД. Повторите попытку позже.\r\nТекст ошибки:\r\n{ex.Message}");
                return false;
            }
            MessageBox.Show("Изменения успешно сохранены в БД");
            isNew = false;
            UpdateButtonsEnabled();
            return true;
        }
    }
}
