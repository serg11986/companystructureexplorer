﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Data.Entity;

namespace CompanyStructure
{
    public class QueryManager
    {

        public DepartmentBriefInfo GetRootInfo()
        {
            using (var context = new CompanyStructureDbContext())
            {
                var deps = context.Departments;

                foreach (var d in deps)
                {
                    if (d.ParentDepartment == null)
                        return (DepartmentBriefInfo)d;
                }

                throw new Exception("Root not found"); //указать точнее тип исключения или создать новый
            }
        }

        public Department GetRoot(bool withEmployees = false)
        {
            using (var context = new CompanyStructureDbContext())
            {
                var deps = withEmployees ? context.Departments.Include(d => d.Empoyee) : context.Departments;

                foreach (var d in deps)
                {
                    if (d.ParentDepartment == null)
                        return d;
                }

                throw new Exception("Root not found"); //указать точнее тип исключения или создать новый
            }
        }

        public Tuple<string, string>[] GetAllDepsIdsAndNames()
        {
            using (var context = new CompanyStructureDbContext())
            {
                return context.Departments.AsEnumerable().Select(d => new Tuple<string, string>(d.ID + "", d.Name)).ToArray();
            }
        }

        public Department[] GetAllDeps()
        {
            using (var context = new CompanyStructureDbContext())
            {
                return context.Departments.Include(d => d.ChildDepartments).ToArray();
            }
        }


        public DepartmentBriefInfo[] GetChildDepsInfos(Guid id)
        {
            using (var context = new CompanyStructureDbContext())
            {
                var dep = context.Departments.Find(id);
                
                return dep.ChildDepartments.Select(d => (DepartmentBriefInfo)d).ToArray();
            }
        }

        public Department GetDepartment(Guid id, bool withEmloyees = false)
        {
            using (var context = new CompanyStructureDbContext())
            {
                return withEmloyees ?
                    context.Departments.Include(d => d.Empoyee).FirstOrDefault(d => d.ID == id) :
                    context.Departments.Find(id);
            }
        }

        public Empoyee GetEmpoyee(decimal id)
        {
            using (var context = new CompanyStructureDbContext())
            {
                return context.Employes.Include(emp => emp.Department).FirstOrDefault(emp => emp.ID == id);
            }
        }

        public void RemoveEmployee(decimal id)
        {
            using (var context = new CompanyStructureDbContext())
            {
                Empoyee emp = context.Employes.Find(id);

                if (emp == null)
                    return;

                context.Employes.Remove(emp);

                context.SaveChanges();
            }
        }

        public void RemoveEmployee(Empoyee emp, CompanyStructureDbContext context)
        {
            if (emp == null)
                return;

            context.Employes.Remove(emp);

            context.SaveChanges();
        }

        public void RemoveEmployees(IEnumerable<Empoyee> emps, CompanyStructureDbContext context)
        {
            if (emps == null)
                return;

            context.Employes.RemoveRange(emps);

            context.SaveChanges();
        }

        public void RemoveEmployees(decimal[] ids)
        {
            using (var context = new CompanyStructureDbContext())
            {
                IEnumerable<Empoyee> emps = context.Employes.Where(emp => ids.Contains(emp.ID));

                context.Employes.RemoveRange(emps);

                context.SaveChanges();
            }
        }

        public void Save(Department department)
        {
            using (var context = new CompanyStructureDbContext())
            {
                Department origin = context.Departments.SingleOrDefault(d => d.ID == department.ID);

                department.CopyTo(origin);

                context.SaveChanges();
            }
        }

        public void Insert(Department newDepartment)
        {
            using (var context = new CompanyStructureDbContext())
            {
                context.Departments.Add(newDepartment);

                context.SaveChanges();
            }
        }

        public void Insert(Empoyee newEmpoyee)
        {
            using (var context = new CompanyStructureDbContext())
            {
                context.Employes.Add(newEmpoyee);

                context.SaveChanges();
            }
        }

        public void RemoveDepartment(Guid id)
        {
            using (var context = new CompanyStructureDbContext())
            {
                var dep = context.Departments.FirstOrDefault(d => d.ID == id);

                RemoveDepartment(dep, context);

                context.SaveChanges();
            }
        }

        public void RemoveDepartment(Department dep, CompanyStructureDbContext context)
        {
            if (dep == null)
                return;
            RemoveEmployees(dep.Empoyee, context);
            List<Department> depsToRemove = dep.ChildDepartments.ToList();
            foreach (Department d in depsToRemove)
            {
                d.ParentDepartment = null;
                RemoveDepartment(d, context);
            }

            context.Departments.Remove(dep);
        }

        public void Save(Empoyee empoyee)
        {
            using (var context = new CompanyStructureDbContext())
            {
                Empoyee origin = context.Employes.SingleOrDefault(e => e.ID == empoyee.ID);

                empoyee.CopyTo(origin);

                context.SaveChanges();
            }
        }
    }
}
