﻿namespace CompanyStructure
{
    partial class EmployeeEditForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.tbName = new System.Windows.Forms.TextBox();
            this.tbPatr = new System.Windows.Forms.TextBox();
            this.tbSurname = new System.Windows.Forms.TextBox();
            this.dateTimePicker1 = new System.Windows.Forms.DateTimePicker();
            this.tbPosition = new System.Windows.Forms.TextBox();
            this.tbDocSer = new System.Windows.Forms.TextBox();
            this.tbDocNum = new System.Windows.Forms.TextBox();
            this.cbDep = new System.Windows.Forms.ComboBox();
            this.label1 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.label4 = new System.Windows.Forms.Label();
            this.label5 = new System.Windows.Forms.Label();
            this.lblAge = new System.Windows.Forms.Label();
            this.label6 = new System.Windows.Forms.Label();
            this.lblDateError = new System.Windows.Forms.Label();
            this.btnUndoChanges = new System.Windows.Forms.Button();
            this.btnSaveChanges = new System.Windows.Forms.Button();
            this.btnSaveQuit = new System.Windows.Forms.Button();
            this.btnQuitWithoutSave = new System.Windows.Forms.Button();
            this.lblErrPos = new System.Windows.Forms.Label();
            this.lblErrName = new System.Windows.Forms.Label();
            this.lblErrPatr = new System.Windows.Forms.Label();
            this.lblErrSurname = new System.Windows.Forms.Label();
            this.label13 = new System.Windows.Forms.Label();
            this.label14 = new System.Windows.Forms.Label();
            this.label15 = new System.Windows.Forms.Label();
            this.lblErrDocSer = new System.Windows.Forms.Label();
            this.lblErrDocNum = new System.Windows.Forms.Label();
            this.SuspendLayout();
            // 
            // tbName
            // 
            this.tbName.Location = new System.Drawing.Point(36, 86);
            this.tbName.Margin = new System.Windows.Forms.Padding(4);
            this.tbName.MaxLength = 50;
            this.tbName.Name = "tbName";
            this.tbName.Size = new System.Drawing.Size(451, 24);
            this.tbName.TabIndex = 0;
            this.tbName.TextChanged += new System.EventHandler(this.tbName_TextChanged);
            // 
            // tbPatr
            // 
            this.tbPatr.Location = new System.Drawing.Point(36, 194);
            this.tbPatr.Margin = new System.Windows.Forms.Padding(4);
            this.tbPatr.MaxLength = 50;
            this.tbPatr.Name = "tbPatr";
            this.tbPatr.Size = new System.Drawing.Size(451, 24);
            this.tbPatr.TabIndex = 1;
            this.tbPatr.TextChanged += new System.EventHandler(this.tbPatr_TextChanged);
            // 
            // tbSurname
            // 
            this.tbSurname.Location = new System.Drawing.Point(36, 303);
            this.tbSurname.Margin = new System.Windows.Forms.Padding(4);
            this.tbSurname.MaxLength = 50;
            this.tbSurname.Name = "tbSurname";
            this.tbSurname.Size = new System.Drawing.Size(451, 24);
            this.tbSurname.TabIndex = 2;
            this.tbSurname.TextChanged += new System.EventHandler(this.tbSurname_TextChanged);
            // 
            // dateTimePicker1
            // 
            this.dateTimePicker1.Location = new System.Drawing.Point(36, 423);
            this.dateTimePicker1.Margin = new System.Windows.Forms.Padding(4);
            this.dateTimePicker1.Name = "dateTimePicker1";
            this.dateTimePicker1.Size = new System.Drawing.Size(224, 24);
            this.dateTimePicker1.TabIndex = 3;
            this.dateTimePicker1.Value = new System.DateTime(1984, 1, 1, 0, 0, 0, 0);
            this.dateTimePicker1.ValueChanged += new System.EventHandler(this.dateTimePicker1_ValueChanged);
            // 
            // tbPosition
            // 
            this.tbPosition.Location = new System.Drawing.Point(771, 194);
            this.tbPosition.Margin = new System.Windows.Forms.Padding(4);
            this.tbPosition.MaxLength = 50;
            this.tbPosition.Name = "tbPosition";
            this.tbPosition.Size = new System.Drawing.Size(524, 24);
            this.tbPosition.TabIndex = 4;
            this.tbPosition.TextChanged += new System.EventHandler(this.tbPosition_TextChanged);
            // 
            // tbDocSer
            // 
            this.tbDocSer.Location = new System.Drawing.Point(771, 338);
            this.tbDocSer.Margin = new System.Windows.Forms.Padding(4);
            this.tbDocSer.MaxLength = 4;
            this.tbDocSer.Name = "tbDocSer";
            this.tbDocSer.Size = new System.Drawing.Size(136, 24);
            this.tbDocSer.TabIndex = 5;
            this.tbDocSer.TextChanged += new System.EventHandler(this.tbDocSer_TextChanged);
            // 
            // tbDocNum
            // 
            this.tbDocNum.Location = new System.Drawing.Point(768, 433);
            this.tbDocNum.Margin = new System.Windows.Forms.Padding(4);
            this.tbDocNum.MaxLength = 6;
            this.tbDocNum.Name = "tbDocNum";
            this.tbDocNum.Size = new System.Drawing.Size(196, 24);
            this.tbDocNum.TabIndex = 6;
            this.tbDocNum.TextChanged += new System.EventHandler(this.tbDocNum_TextChanged);
            this.tbDocNum.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.tbDocNum_KeyPress);
            // 
            // cbDep
            // 
            this.cbDep.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cbDep.FlatStyle = System.Windows.Forms.FlatStyle.Popup;
            this.cbDep.FormattingEnabled = true;
            this.cbDep.Location = new System.Drawing.Point(771, 86);
            this.cbDep.Margin = new System.Windows.Forms.Padding(4);
            this.cbDep.Name = "cbDep";
            this.cbDep.Size = new System.Drawing.Size(524, 26);
            this.cbDep.TabIndex = 7;
            this.cbDep.SelectedIndexChanged += new System.EventHandler(this.cbDep_SelectedIndexChanged);
            this.cbDep.Format += new System.Windows.Forms.ListControlConvertEventHandler(this.cbDep_Format);
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(812, 314);
            this.label1.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(51, 18);
            this.label1.TabIndex = 8;
            this.label1.Text = "Серия";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(812, 411);
            this.label2.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(55, 18);
            this.label2.TabIndex = 9;
            this.label2.Text = "Номер";
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(1006, 172);
            this.label3.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(89, 18);
            this.label3.TabIndex = 10;
            this.label3.Text = "Должность";
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(1019, 56);
            this.label4.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(53, 18);
            this.label4.TabIndex = 11;
            this.label4.Text = "Отдел";
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Location = new System.Drawing.Point(951, 278);
            this.label5.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(199, 18);
            this.label5.TabIndex = 12;
            this.label5.Text = "Документ (необязательно)";
            // 
            // lblAge
            // 
            this.lblAge.AutoSize = true;
            this.lblAge.ForeColor = System.Drawing.Color.Blue;
            this.lblAge.Location = new System.Drawing.Point(36, 455);
            this.lblAge.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.lblAge.Name = "lblAge";
            this.lblAge.Size = new System.Drawing.Size(47, 18);
            this.lblAge.TabIndex = 14;
            this.lblAge.Text = "lblAge";
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Location = new System.Drawing.Point(88, 396);
            this.label6.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(116, 18);
            this.label6.TabIndex = 15;
            this.label6.Text = "Дата рождения";
            // 
            // lblDateError
            // 
            this.lblDateError.AutoSize = true;
            this.lblDateError.ForeColor = System.Drawing.Color.Red;
            this.lblDateError.Location = new System.Drawing.Point(36, 455);
            this.lblDateError.Name = "lblDateError";
            this.lblDateError.Size = new System.Drawing.Size(0, 18);
            this.lblDateError.TabIndex = 16;
            // 
            // btnUndoChanges
            // 
            this.btnUndoChanges.Location = new System.Drawing.Point(6, 538);
            this.btnUndoChanges.Name = "btnUndoChanges";
            this.btnUndoChanges.Size = new System.Drawing.Size(1523, 30);
            this.btnUndoChanges.TabIndex = 18;
            this.btnUndoChanges.Text = "Отменить изменения";
            this.btnUndoChanges.UseVisualStyleBackColor = true;
            this.btnUndoChanges.Click += new System.EventHandler(this.btnUndoChanges_Click);
            // 
            // btnSaveChanges
            // 
            this.btnSaveChanges.Location = new System.Drawing.Point(6, 574);
            this.btnSaveChanges.Name = "btnSaveChanges";
            this.btnSaveChanges.Size = new System.Drawing.Size(1523, 30);
            this.btnSaveChanges.TabIndex = 19;
            this.btnSaveChanges.Text = "Сохранить изменения";
            this.btnSaveChanges.UseVisualStyleBackColor = true;
            this.btnSaveChanges.Click += new System.EventHandler(this.btnSaveChanges_Click);
            // 
            // btnSaveQuit
            // 
            this.btnSaveQuit.Location = new System.Drawing.Point(6, 610);
            this.btnSaveQuit.Name = "btnSaveQuit";
            this.btnSaveQuit.Size = new System.Drawing.Size(1523, 30);
            this.btnSaveQuit.TabIndex = 20;
            this.btnSaveQuit.Text = "Сохранить и выйти";
            this.btnSaveQuit.UseVisualStyleBackColor = true;
            this.btnSaveQuit.Click += new System.EventHandler(this.btnSaveQuit_Click);
            // 
            // btnQuitWithoutSave
            // 
            this.btnQuitWithoutSave.Location = new System.Drawing.Point(6, 648);
            this.btnQuitWithoutSave.Name = "btnQuitWithoutSave";
            this.btnQuitWithoutSave.Size = new System.Drawing.Size(1523, 30);
            this.btnQuitWithoutSave.TabIndex = 21;
            this.btnQuitWithoutSave.Text = "Выйти без сохранения";
            this.btnQuitWithoutSave.UseVisualStyleBackColor = true;
            this.btnQuitWithoutSave.Click += new System.EventHandler(this.btnQuitWithoutSave_Click);
            // 
            // lblErrPos
            // 
            this.lblErrPos.AutoSize = true;
            this.lblErrPos.ForeColor = System.Drawing.Color.Red;
            this.lblErrPos.Location = new System.Drawing.Point(771, 228);
            this.lblErrPos.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.lblErrPos.Name = "lblErrPos";
            this.lblErrPos.Size = new System.Drawing.Size(0, 18);
            this.lblErrPos.TabIndex = 24;
            // 
            // lblErrName
            // 
            this.lblErrName.AutoSize = true;
            this.lblErrName.ForeColor = System.Drawing.Color.Red;
            this.lblErrName.Location = new System.Drawing.Point(36, 123);
            this.lblErrName.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.lblErrName.Name = "lblErrName";
            this.lblErrName.Size = new System.Drawing.Size(0, 18);
            this.lblErrName.TabIndex = 25;
            // 
            // lblErrPatr
            // 
            this.lblErrPatr.AutoSize = true;
            this.lblErrPatr.ForeColor = System.Drawing.Color.Red;
            this.lblErrPatr.Location = new System.Drawing.Point(36, 228);
            this.lblErrPatr.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.lblErrPatr.Name = "lblErrPatr";
            this.lblErrPatr.Size = new System.Drawing.Size(0, 18);
            this.lblErrPatr.TabIndex = 26;
            // 
            // lblErrSurname
            // 
            this.lblErrSurname.AutoSize = true;
            this.lblErrSurname.ForeColor = System.Drawing.Color.Red;
            this.lblErrSurname.Location = new System.Drawing.Point(36, 340);
            this.lblErrSurname.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.lblErrSurname.Name = "lblErrSurname";
            this.lblErrSurname.Size = new System.Drawing.Size(0, 18);
            this.lblErrSurname.TabIndex = 27;
            // 
            // label13
            // 
            this.label13.AutoSize = true;
            this.label13.Location = new System.Drawing.Point(239, 56);
            this.label13.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label13.Name = "label13";
            this.label13.Size = new System.Drawing.Size(38, 18);
            this.label13.TabIndex = 28;
            this.label13.Text = "Имя";
            // 
            // label14
            // 
            this.label14.AutoSize = true;
            this.label14.Location = new System.Drawing.Point(168, 166);
            this.label14.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label14.Name = "label14";
            this.label14.Size = new System.Drawing.Size(196, 18);
            this.label14.TabIndex = 29;
            this.label14.Text = "Отчество (необязательно)";
            // 
            // label15
            // 
            this.label15.AutoSize = true;
            this.label15.Location = new System.Drawing.Point(218, 278);
            this.label15.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label15.Name = "label15";
            this.label15.Size = new System.Drawing.Size(73, 18);
            this.label15.TabIndex = 30;
            this.label15.Text = "Фамилия";
            // 
            // lblErrDocSer
            // 
            this.lblErrDocSer.AutoSize = true;
            this.lblErrDocSer.ForeColor = System.Drawing.Color.Red;
            this.lblErrDocSer.Location = new System.Drawing.Point(771, 372);
            this.lblErrDocSer.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.lblErrDocSer.Name = "lblErrDocSer";
            this.lblErrDocSer.Size = new System.Drawing.Size(0, 18);
            this.lblErrDocSer.TabIndex = 31;
            // 
            // lblErrDocNum
            // 
            this.lblErrDocNum.AutoSize = true;
            this.lblErrDocNum.ForeColor = System.Drawing.Color.Red;
            this.lblErrDocNum.Location = new System.Drawing.Point(768, 473);
            this.lblErrDocNum.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.lblErrDocNum.Name = "lblErrDocNum";
            this.lblErrDocNum.Size = new System.Drawing.Size(0, 18);
            this.lblErrDocNum.TabIndex = 32;
            // 
            // EmployeeEditForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(9F, 18F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(1541, 700);
            this.Controls.Add(this.lblErrDocNum);
            this.Controls.Add(this.lblErrDocSer);
            this.Controls.Add(this.label15);
            this.Controls.Add(this.label14);
            this.Controls.Add(this.label13);
            this.Controls.Add(this.lblErrSurname);
            this.Controls.Add(this.lblErrPatr);
            this.Controls.Add(this.lblErrName);
            this.Controls.Add(this.lblErrPos);
            this.Controls.Add(this.btnQuitWithoutSave);
            this.Controls.Add(this.btnSaveQuit);
            this.Controls.Add(this.btnSaveChanges);
            this.Controls.Add(this.btnUndoChanges);
            this.Controls.Add(this.lblDateError);
            this.Controls.Add(this.label6);
            this.Controls.Add(this.lblAge);
            this.Controls.Add(this.label5);
            this.Controls.Add(this.label4);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.cbDep);
            this.Controls.Add(this.tbDocNum);
            this.Controls.Add(this.tbDocSer);
            this.Controls.Add(this.tbPosition);
            this.Controls.Add(this.dateTimePicker1);
            this.Controls.Add(this.tbSurname);
            this.Controls.Add(this.tbPatr);
            this.Controls.Add(this.tbName);
            this.Font = new System.Drawing.Font("Microsoft Sans Serif", 11F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedDialog;
            this.Margin = new System.Windows.Forms.Padding(4);
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            this.Name = "EmployeeEditForm";
            this.ShowIcon = false;
            this.ShowInTaskbar = false;
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.TextBox tbName;
        private System.Windows.Forms.TextBox tbPatr;
        private System.Windows.Forms.TextBox tbSurname;
        private System.Windows.Forms.DateTimePicker dateTimePicker1;
        private System.Windows.Forms.TextBox tbPosition;
        private System.Windows.Forms.TextBox tbDocSer;
        private System.Windows.Forms.TextBox tbDocNum;
        private System.Windows.Forms.ComboBox cbDep;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.Label lblAge;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.Label lblDateError;
        private System.Windows.Forms.Button btnUndoChanges;
        private System.Windows.Forms.Button btnSaveChanges;
        private System.Windows.Forms.Button btnSaveQuit;
        private System.Windows.Forms.Button btnQuitWithoutSave;
        private System.Windows.Forms.Label lblErrPos;
        private System.Windows.Forms.Label lblErrName;
        private System.Windows.Forms.Label lblErrPatr;
        private System.Windows.Forms.Label lblErrSurname;
        private System.Windows.Forms.Label label13;
        private System.Windows.Forms.Label label14;
        private System.Windows.Forms.Label label15;
        private System.Windows.Forms.Label lblErrDocSer;
        private System.Windows.Forms.Label lblErrDocNum;
    }
}