﻿namespace CompanyStructure
{
    partial class MainForm
    {
        /// <summary>
        /// Обязательная переменная конструктора.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Освободить все используемые ресурсы.
        /// </summary>
        /// <param name="disposing">истинно, если управляемый ресурс должен быть удален; иначе ложно.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Код, автоматически созданный конструктором форм Windows

        /// <summary>
        /// Требуемый метод для поддержки конструктора — не изменяйте 
        /// содержимое этого метода с помощью редактора кода.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(MainForm));
            this.panel1 = new System.Windows.Forms.Panel();
            this.btnRemoveCurDep = new System.Windows.Forms.Button();
            this.btnAddNewDep = new System.Windows.Forms.Button();
            this.label1 = new System.Windows.Forms.Label();
            this.lblFreq2 = new System.Windows.Forms.Label();
            this.lblFreq1 = new System.Windows.Forms.Label();
            this.btnUp = new System.Windows.Forms.Button();
            this.nudFreq = new System.Windows.Forms.NumericUpDown();
            this.treeView = new System.Windows.Forms.TreeView();
            this.btnLeft = new System.Windows.Forms.Button();
            this.cbAutoUpdate = new System.Windows.Forms.CheckBox();
            this.btnDown = new System.Windows.Forms.Button();
            this.btnUpdateInfo = new System.Windows.Forms.Button();
            this.btnRight = new System.Windows.Forms.Button();
            this.panel2 = new System.Windows.Forms.Panel();
            this.panel3 = new System.Windows.Forms.Panel();
            this.lblErrCode = new System.Windows.Forms.Label();
            this.lblErrName = new System.Windows.Forms.Label();
            this.cbCurDepParentDep = new System.Windows.Forms.ComboBox();
            this.tbCurDepCode = new System.Windows.Forms.TextBox();
            this.label5 = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.tbCurDepName = new System.Windows.Forms.TextBox();
            this.btnSaveDepartmentInfoChanges = new System.Windows.Forms.Button();
            this.panel5 = new System.Windows.Forms.Panel();
            this.btnRemoveEmps = new System.Windows.Forms.Button();
            this.dataGridView1 = new System.Windows.Forms.DataGridView();
            this.Column0Id = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Column1Name = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Column2Pos = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Column3Age = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Column4Edit = new System.Windows.Forms.DataGridViewButtonColumn();
            this.Column5Remove = new System.Windows.Forms.DataGridViewButtonColumn();
            this.Column6CB = new System.Windows.Forms.DataGridViewCheckBoxColumn();
            this.btnAddEmployee = new System.Windows.Forms.Button();
            this.label6 = new System.Windows.Forms.Label();
            this.label4 = new System.Windows.Forms.Label();
            this.btnUndoDepartmentInfoChanges = new System.Windows.Forms.Button();
            this.label2 = new System.Windows.Forms.Label();
            this.cbLazyLoad = new System.Windows.Forms.CheckBox();
            this.cbAskDel = new System.Windows.Forms.CheckBox();
            this.label7 = new System.Windows.Forms.Label();
            this.cbActNotSave = new System.Windows.Forms.ComboBox();
            this.panel1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.nudFreq)).BeginInit();
            this.panel2.SuspendLayout();
            this.panel3.SuspendLayout();
            this.panel5.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dataGridView1)).BeginInit();
            this.SuspendLayout();
            // 
            // panel1
            // 
            this.panel1.BackColor = System.Drawing.SystemColors.Control;
            this.panel1.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.panel1.Controls.Add(this.btnRemoveCurDep);
            this.panel1.Controls.Add(this.btnAddNewDep);
            this.panel1.Controls.Add(this.label1);
            this.panel1.Controls.Add(this.lblFreq2);
            this.panel1.Controls.Add(this.lblFreq1);
            this.panel1.Controls.Add(this.btnUp);
            this.panel1.Controls.Add(this.nudFreq);
            this.panel1.Controls.Add(this.treeView);
            this.panel1.Controls.Add(this.btnLeft);
            this.panel1.Controls.Add(this.cbAutoUpdate);
            this.panel1.Controls.Add(this.btnDown);
            this.panel1.Controls.Add(this.btnUpdateInfo);
            this.panel1.Controls.Add(this.btnRight);
            this.panel1.Location = new System.Drawing.Point(16, 3);
            this.panel1.Margin = new System.Windows.Forms.Padding(4);
            this.panel1.Name = "panel1";
            this.panel1.Size = new System.Drawing.Size(443, 674);
            this.panel1.TabIndex = 1;
            // 
            // btnRemoveCurDep
            // 
            this.btnRemoveCurDep.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.btnRemoveCurDep.Location = new System.Drawing.Point(21, 365);
            this.btnRemoveCurDep.Name = "btnRemoveCurDep";
            this.btnRemoveCurDep.Size = new System.Drawing.Size(403, 37);
            this.btnRemoveCurDep.TabIndex = 22;
            this.btnRemoveCurDep.Text = "Удалить выбранный отдел";
            this.btnRemoveCurDep.UseVisualStyleBackColor = true;
            this.btnRemoveCurDep.Click += new System.EventHandler(this.btnRemoveCurDep_Click);
            // 
            // btnAddNewDep
            // 
            this.btnAddNewDep.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.btnAddNewDep.Location = new System.Drawing.Point(21, 408);
            this.btnAddNewDep.Name = "btnAddNewDep";
            this.btnAddNewDep.Size = new System.Drawing.Size(403, 37);
            this.btnAddNewDep.TabIndex = 21;
            this.btnAddNewDep.Text = "Добавить новый отдел";
            this.btnAddNewDep.UseVisualStyleBackColor = true;
            this.btnAddNewDep.Click += new System.EventHandler(this.btnAddNewDep_Click);
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("Microsoft Sans Serif", 14F, System.Drawing.FontStyle.Italic, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.label1.Location = new System.Drawing.Point(128, 6);
            this.label1.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(194, 24);
            this.label1.TabIndex = 1;
            this.label1.Text = "Структура компании";
            // 
            // lblFreq2
            // 
            this.lblFreq2.AutoSize = true;
            this.lblFreq2.Location = new System.Drawing.Point(382, 646);
            this.lblFreq2.Name = "lblFreq2";
            this.lblFreq2.Size = new System.Drawing.Size(42, 17);
            this.lblFreq2.TabIndex = 7;
            this.lblFreq2.Text = "мин.)";
            // 
            // lblFreq1
            // 
            this.lblFreq1.AutoSize = true;
            this.lblFreq1.Location = new System.Drawing.Point(144, 647);
            this.lblFreq1.Name = "lblFreq1";
            this.lblFreq1.Size = new System.Drawing.Size(108, 17);
            this.lblFreq1.TabIndex = 6;
            this.lblFreq1.Text = "(частота: раз в";
            // 
            // btnUp
            // 
            this.btnUp.Image = ((System.Drawing.Image)(resources.GetObject("btnUp.Image")));
            this.btnUp.Location = new System.Drawing.Point(181, 452);
            this.btnUp.Margin = new System.Windows.Forms.Padding(4);
            this.btnUp.Name = "btnUp";
            this.btnUp.Size = new System.Drawing.Size(69, 71);
            this.btnUp.TabIndex = 8;
            this.btnUp.UseVisualStyleBackColor = true;
            this.btnUp.Click += new System.EventHandler(this.btnUp_Click);
            this.btnUp.KeyDown += new System.Windows.Forms.KeyEventHandler(this.FormKeyDown);
            // 
            // nudFreq
            // 
            this.nudFreq.DecimalPlaces = 1;
            this.nudFreq.Increment = new decimal(new int[] {
            5,
            0,
            0,
            65536});
            this.nudFreq.Location = new System.Drawing.Point(255, 645);
            this.nudFreq.Maximum = new decimal(new int[] {
            30,
            0,
            0,
            0});
            this.nudFreq.Minimum = new decimal(new int[] {
            5,
            0,
            0,
            65536});
            this.nudFreq.Name = "nudFreq";
            this.nudFreq.Size = new System.Drawing.Size(120, 23);
            this.nudFreq.TabIndex = 5;
            this.nudFreq.Value = new decimal(new int[] {
            5,
            0,
            0,
            0});
            this.nudFreq.ValueChanged += new System.EventHandler(this.nudFreq_ValueChanged);
            // 
            // treeView
            // 
            this.treeView.HideSelection = false;
            this.treeView.Location = new System.Drawing.Point(21, 34);
            this.treeView.Margin = new System.Windows.Forms.Padding(4);
            this.treeView.Name = "treeView";
            this.treeView.Size = new System.Drawing.Size(403, 324);
            this.treeView.TabIndex = 0;
            this.treeView.BeforeCollapse += new System.Windows.Forms.TreeViewCancelEventHandler(this.treeView_BeforeCollapse);
            this.treeView.BeforeExpand += new System.Windows.Forms.TreeViewCancelEventHandler(this.treeView_BeforeExpand);
            this.treeView.BeforeSelect += new System.Windows.Forms.TreeViewCancelEventHandler(this.treeView_BeforeSelect);
            this.treeView.AfterSelect += new System.Windows.Forms.TreeViewEventHandler(this.treeView_AfterSelect);
            // 
            // btnLeft
            // 
            this.btnLeft.Image = ((System.Drawing.Image)(resources.GetObject("btnLeft.Image")));
            this.btnLeft.Location = new System.Drawing.Point(97, 494);
            this.btnLeft.Margin = new System.Windows.Forms.Padding(4);
            this.btnLeft.Name = "btnLeft";
            this.btnLeft.Size = new System.Drawing.Size(75, 67);
            this.btnLeft.TabIndex = 9;
            this.btnLeft.UseVisualStyleBackColor = true;
            this.btnLeft.Click += new System.EventHandler(this.btnLeft_Click);
            // 
            // cbAutoUpdate
            // 
            this.cbAutoUpdate.AutoSize = true;
            this.cbAutoUpdate.Checked = true;
            this.cbAutoUpdate.CheckState = System.Windows.Forms.CheckState.Checked;
            this.cbAutoUpdate.Location = new System.Drawing.Point(3, 646);
            this.cbAutoUpdate.Name = "cbAutoUpdate";
            this.cbAutoUpdate.Size = new System.Drawing.Size(137, 21);
            this.cbAutoUpdate.TabIndex = 4;
            this.cbAutoUpdate.Text = "Автообновление";
            this.cbAutoUpdate.UseVisualStyleBackColor = true;
            this.cbAutoUpdate.CheckedChanged += new System.EventHandler(this.cbAutoUpdate_CheckedChanged);
            // 
            // btnDown
            // 
            this.btnDown.Image = ((System.Drawing.Image)(resources.GetObject("btnDown.Image")));
            this.btnDown.Location = new System.Drawing.Point(176, 531);
            this.btnDown.Margin = new System.Windows.Forms.Padding(4);
            this.btnDown.Name = "btnDown";
            this.btnDown.Size = new System.Drawing.Size(69, 71);
            this.btnDown.TabIndex = 7;
            this.btnDown.UseVisualStyleBackColor = true;
            this.btnDown.Click += new System.EventHandler(this.btnDown_Click);
            // 
            // btnUpdateInfo
            // 
            this.btnUpdateInfo.Location = new System.Drawing.Point(3, 608);
            this.btnUpdateInfo.Name = "btnUpdateInfo";
            this.btnUpdateInfo.Size = new System.Drawing.Size(421, 33);
            this.btnUpdateInfo.TabIndex = 3;
            this.btnUpdateInfo.Text = "Обновить информацию из БД";
            this.btnUpdateInfo.UseVisualStyleBackColor = true;
            this.btnUpdateInfo.Click += new System.EventHandler(this.btnUpdateInfo_Click);
            // 
            // btnRight
            // 
            this.btnRight.Image = ((System.Drawing.Image)(resources.GetObject("btnRight.Image")));
            this.btnRight.Location = new System.Drawing.Point(258, 498);
            this.btnRight.Margin = new System.Windows.Forms.Padding(4);
            this.btnRight.Name = "btnRight";
            this.btnRight.Size = new System.Drawing.Size(75, 67);
            this.btnRight.TabIndex = 10;
            this.btnRight.UseVisualStyleBackColor = true;
            this.btnRight.Click += new System.EventHandler(this.btnRight_Click);
            // 
            // panel2
            // 
            this.panel2.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.panel2.Controls.Add(this.panel3);
            this.panel2.Controls.Add(this.label2);
            this.panel2.Location = new System.Drawing.Point(467, 3);
            this.panel2.Margin = new System.Windows.Forms.Padding(4);
            this.panel2.Name = "panel2";
            this.panel2.Size = new System.Drawing.Size(979, 674);
            this.panel2.TabIndex = 2;
            // 
            // panel3
            // 
            this.panel3.BackColor = System.Drawing.SystemColors.ControlLight;
            this.panel3.Controls.Add(this.lblErrCode);
            this.panel3.Controls.Add(this.lblErrName);
            this.panel3.Controls.Add(this.cbCurDepParentDep);
            this.panel3.Controls.Add(this.tbCurDepCode);
            this.panel3.Controls.Add(this.label5);
            this.panel3.Controls.Add(this.label3);
            this.panel3.Controls.Add(this.tbCurDepName);
            this.panel3.Controls.Add(this.btnSaveDepartmentInfoChanges);
            this.panel3.Controls.Add(this.panel5);
            this.panel3.Controls.Add(this.label4);
            this.panel3.Controls.Add(this.btnUndoDepartmentInfoChanges);
            this.panel3.Location = new System.Drawing.Point(6, 34);
            this.panel3.Margin = new System.Windows.Forms.Padding(4);
            this.panel3.Name = "panel3";
            this.panel3.Size = new System.Drawing.Size(965, 634);
            this.panel3.TabIndex = 11;
            // 
            // lblErrCode
            // 
            this.lblErrCode.AutoSize = true;
            this.lblErrCode.ForeColor = System.Drawing.Color.Red;
            this.lblErrCode.Location = new System.Drawing.Point(435, 67);
            this.lblErrCode.Name = "lblErrCode";
            this.lblErrCode.Size = new System.Drawing.Size(0, 17);
            this.lblErrCode.TabIndex = 19;
            // 
            // lblErrName
            // 
            this.lblErrName.AutoSize = true;
            this.lblErrName.ForeColor = System.Drawing.Color.Red;
            this.lblErrName.Location = new System.Drawing.Point(6, 67);
            this.lblErrName.Name = "lblErrName";
            this.lblErrName.Size = new System.Drawing.Size(0, 17);
            this.lblErrName.TabIndex = 18;
            // 
            // cbCurDepParentDep
            // 
            this.cbCurDepParentDep.BackColor = System.Drawing.SystemColors.Window;
            this.cbCurDepParentDep.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cbCurDepParentDep.FlatStyle = System.Windows.Forms.FlatStyle.Popup;
            this.cbCurDepParentDep.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.cbCurDepParentDep.FormattingEnabled = true;
            this.cbCurDepParentDep.Location = new System.Drawing.Point(541, 37);
            this.cbCurDepParentDep.Margin = new System.Windows.Forms.Padding(4);
            this.cbCurDepParentDep.Name = "cbCurDepParentDep";
            this.cbCurDepParentDep.Size = new System.Drawing.Size(420, 28);
            this.cbCurDepParentDep.TabIndex = 11;
            this.cbCurDepParentDep.SelectedIndexChanged += new System.EventHandler(this.cbCurDepParentDep_SelectedIndexChanged);
            this.cbCurDepParentDep.Format += new System.Windows.Forms.ListControlConvertEventHandler(this.cbCurDepParentDep_Format);
            // 
            // tbCurDepCode
            // 
            this.tbCurDepCode.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.tbCurDepCode.Location = new System.Drawing.Point(431, 38);
            this.tbCurDepCode.Margin = new System.Windows.Forms.Padding(4);
            this.tbCurDepCode.MaxLength = 10;
            this.tbCurDepCode.Name = "tbCurDepCode";
            this.tbCurDepCode.Size = new System.Drawing.Size(102, 26);
            this.tbCurDepCode.TabIndex = 4;
            this.tbCurDepCode.TextChanged += new System.EventHandler(this.tbCurDepCode_TextChanged);
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.label5.Location = new System.Drawing.Point(650, 13);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(171, 20);
            this.label5.TabIndex = 14;
            this.label5.Text = "Родительский отдел";
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.label3.Location = new System.Drawing.Point(162, 12);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(122, 20);
            this.label3.TabIndex = 12;
            this.label3.Text = "Наименование";
            // 
            // tbCurDepName
            // 
            this.tbCurDepName.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.tbCurDepName.Location = new System.Drawing.Point(3, 37);
            this.tbCurDepName.Margin = new System.Windows.Forms.Padding(4);
            this.tbCurDepName.MaxLength = 50;
            this.tbCurDepName.Name = "tbCurDepName";
            this.tbCurDepName.Size = new System.Drawing.Size(420, 26);
            this.tbCurDepName.TabIndex = 3;
            this.tbCurDepName.TextChanged += new System.EventHandler(this.tbCurDepName_TextChanged);
            this.tbCurDepName.KeyDown += new System.Windows.Forms.KeyEventHandler(this.FormKeyDown);
            // 
            // btnSaveDepartmentInfoChanges
            // 
            this.btnSaveDepartmentInfoChanges.BackColor = System.Drawing.SystemColors.ActiveCaption;
            this.btnSaveDepartmentInfoChanges.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.btnSaveDepartmentInfoChanges.Location = new System.Drawing.Point(0, 581);
            this.btnSaveDepartmentInfoChanges.Name = "btnSaveDepartmentInfoChanges";
            this.btnSaveDepartmentInfoChanges.Size = new System.Drawing.Size(965, 44);
            this.btnSaveDepartmentInfoChanges.TabIndex = 15;
            this.btnSaveDepartmentInfoChanges.Text = "Сохранить изменения";
            this.btnSaveDepartmentInfoChanges.UseVisualStyleBackColor = false;
            this.btnSaveDepartmentInfoChanges.Click += new System.EventHandler(this.btnSaveDepartmentInfoChanges_Click);
            // 
            // panel5
            // 
            this.panel5.BackColor = System.Drawing.SystemColors.InactiveBorder;
            this.panel5.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.panel5.Controls.Add(this.btnRemoveEmps);
            this.panel5.Controls.Add(this.dataGridView1);
            this.panel5.Controls.Add(this.btnAddEmployee);
            this.panel5.Controls.Add(this.label6);
            this.panel5.Location = new System.Drawing.Point(3, 91);
            this.panel5.Name = "panel5";
            this.panel5.Size = new System.Drawing.Size(958, 423);
            this.panel5.TabIndex = 3;
            // 
            // btnRemoveEmps
            // 
            this.btnRemoveEmps.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.btnRemoveEmps.Location = new System.Drawing.Point(3, 332);
            this.btnRemoveEmps.Name = "btnRemoveEmps";
            this.btnRemoveEmps.Size = new System.Drawing.Size(948, 37);
            this.btnRemoveEmps.TabIndex = 20;
            this.btnRemoveEmps.Text = "Удалить выбранных сотрудников";
            this.btnRemoveEmps.UseVisualStyleBackColor = true;
            this.btnRemoveEmps.Click += new System.EventHandler(this.btnRemoveEmps_Click);
            // 
            // dataGridView1
            // 
            this.dataGridView1.AllowUserToAddRows = false;
            this.dataGridView1.AllowUserToDeleteRows = false;
            this.dataGridView1.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dataGridView1.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.Column0Id,
            this.Column1Name,
            this.Column2Pos,
            this.Column3Age,
            this.Column4Edit,
            this.Column5Remove,
            this.Column6CB});
            this.dataGridView1.Location = new System.Drawing.Point(3, 3);
            this.dataGridView1.Name = "dataGridView1";
            this.dataGridView1.Size = new System.Drawing.Size(948, 282);
            this.dataGridView1.TabIndex = 19;
            this.dataGridView1.CellContentClick += new System.Windows.Forms.DataGridViewCellEventHandler(this.dataGridView1_CellContentClick);
            this.dataGridView1.CellValueChanged += new System.Windows.Forms.DataGridViewCellEventHandler(this.dataGridView1_CellValueChanged);
            // 
            // Column0Id
            // 
            this.Column0Id.HeaderText = "ID";
            this.Column0Id.Name = "Column0Id";
            this.Column0Id.ReadOnly = true;
            this.Column0Id.Visible = false;
            // 
            // Column1Name
            // 
            this.Column1Name.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.AllCells;
            this.Column1Name.HeaderText = "Имя, фамилия";
            this.Column1Name.Name = "Column1Name";
            this.Column1Name.ReadOnly = true;
            this.Column1Name.Width = 117;
            // 
            // Column2Pos
            // 
            this.Column2Pos.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.AllCells;
            this.Column2Pos.HeaderText = "Должность";
            this.Column2Pos.Name = "Column2Pos";
            this.Column2Pos.ReadOnly = true;
            this.Column2Pos.Width = 106;
            // 
            // Column3Age
            // 
            this.Column3Age.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.AllCells;
            this.Column3Age.HeaderText = "Возраст";
            this.Column3Age.Name = "Column3Age";
            this.Column3Age.ReadOnly = true;
            this.Column3Age.Width = 87;
            // 
            // Column4Edit
            // 
            this.Column4Edit.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.AllCells;
            this.Column4Edit.HeaderText = "";
            this.Column4Edit.Name = "Column4Edit";
            this.Column4Edit.Resizable = System.Windows.Forms.DataGridViewTriState.True;
            this.Column4Edit.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.Automatic;
            this.Column4Edit.Text = "Смотреть подробнее/изменить";
            this.Column4Edit.UseColumnTextForButtonValue = true;
            this.Column4Edit.Width = 19;
            // 
            // Column5Remove
            // 
            this.Column5Remove.HeaderText = "";
            this.Column5Remove.Name = "Column5Remove";
            this.Column5Remove.Text = "Удалить";
            this.Column5Remove.UseColumnTextForButtonValue = true;
            // 
            // Column6CB
            // 
            this.Column6CB.HeaderText = "Выбрать для удаления";
            this.Column6CB.Name = "Column6CB";
            // 
            // btnAddEmployee
            // 
            this.btnAddEmployee.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.btnAddEmployee.Location = new System.Drawing.Point(3, 375);
            this.btnAddEmployee.Name = "btnAddEmployee";
            this.btnAddEmployee.Size = new System.Drawing.Size(948, 37);
            this.btnAddEmployee.TabIndex = 18;
            this.btnAddEmployee.Text = "Добавить сотрудника";
            this.btnAddEmployee.UseVisualStyleBackColor = true;
            this.btnAddEmployee.Click += new System.EventHandler(this.btnAddEmployee_Click);
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.label6.Location = new System.Drawing.Point(443, 15);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(142, 17);
            this.label6.TabIndex = 16;
            this.label6.Text = "Список сотрудников";
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.label4.Location = new System.Drawing.Point(389, 12);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(219, 20);
            this.label4.TabIndex = 13;
            this.label4.Text = "Мнемокод (необязательно)";
            // 
            // btnUndoDepartmentInfoChanges
            // 
            this.btnUndoDepartmentInfoChanges.BackColor = System.Drawing.SystemColors.ActiveCaption;
            this.btnUndoDepartmentInfoChanges.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.btnUndoDepartmentInfoChanges.Location = new System.Drawing.Point(0, 527);
            this.btnUndoDepartmentInfoChanges.Name = "btnUndoDepartmentInfoChanges";
            this.btnUndoDepartmentInfoChanges.Size = new System.Drawing.Size(966, 44);
            this.btnUndoDepartmentInfoChanges.TabIndex = 17;
            this.btnUndoDepartmentInfoChanges.Text = "Отменить изменения";
            this.btnUndoDepartmentInfoChanges.UseVisualStyleBackColor = false;
            this.btnUndoDepartmentInfoChanges.Click += new System.EventHandler(this.btnUndoDepartmentInfoChanges_Click);
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Font = new System.Drawing.Font("Microsoft Sans Serif", 14F, System.Drawing.FontStyle.Italic, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.label2.Location = new System.Drawing.Point(374, 6);
            this.label2.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(222, 24);
            this.label2.TabIndex = 2;
            this.label2.Text = "Информация об отделе";
            // 
            // cbLazyLoad
            // 
            this.cbLazyLoad.AutoSize = true;
            this.cbLazyLoad.Location = new System.Drawing.Point(13, 689);
            this.cbLazyLoad.Name = "cbLazyLoad";
            this.cbLazyLoad.Size = new System.Drawing.Size(327, 21);
            this.cbLazyLoad.TabIndex = 8;
            this.cbLazyLoad.Text = "Загрузка отделов по требованию (\"ленивая\")";
            this.cbLazyLoad.UseVisualStyleBackColor = true;
            this.cbLazyLoad.CheckedChanged += new System.EventHandler(this.cbLazyLoad_CheckedChanged);
            // 
            // cbAskDel
            // 
            this.cbAskDel.AutoSize = true;
            this.cbAskDel.Checked = true;
            this.cbAskDel.CheckState = System.Windows.Forms.CheckState.Checked;
            this.cbAskDel.Location = new System.Drawing.Point(1058, 689);
            this.cbAskDel.Name = "cbAskDel";
            this.cbAskDel.Size = new System.Drawing.Size(311, 21);
            this.cbAskDel.TabIndex = 12;
            this.cbAskDel.Text = "Предупреждение при удалении сущностей";
            this.cbAskDel.UseVisualStyleBackColor = true;
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.Location = new System.Drawing.Point(399, 690);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(288, 17);
            this.label7.TabIndex = 13;
            this.label7.Text = "При выходе/смене отдела без сохранения";
            // 
            // cbActNotSave
            // 
            this.cbActNotSave.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cbActNotSave.FormattingEnabled = true;
            this.cbActNotSave.Items.AddRange(new object[] {
            "Спрашивать",
            "Сохранять, не спрашивая",
            "Не сохранять, не спрашивая"});
            this.cbActNotSave.Location = new System.Drawing.Point(708, 687);
            this.cbActNotSave.Name = "cbActNotSave";
            this.cbActNotSave.Size = new System.Drawing.Size(300, 24);
            this.cbActNotSave.TabIndex = 14;
            // 
            // MainForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(8F, 16F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(1459, 722);
            this.Controls.Add(this.cbActNotSave);
            this.Controls.Add(this.label7);
            this.Controls.Add(this.cbAskDel);
            this.Controls.Add(this.cbLazyLoad);
            this.Controls.Add(this.panel2);
            this.Controls.Add(this.panel1);
            this.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle;
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.KeyPreview = true;
            this.Margin = new System.Windows.Forms.Padding(4);
            this.MaximizeBox = false;
            this.Name = "MainForm";
            this.Text = "Company Structure Explorer";
            this.FormClosing += new System.Windows.Forms.FormClosingEventHandler(this.MainForm_FormClosing);
            this.KeyDown += new System.Windows.Forms.KeyEventHandler(this.FormKeyDown);
            this.panel1.ResumeLayout(false);
            this.panel1.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.nudFreq)).EndInit();
            this.panel2.ResumeLayout(false);
            this.panel2.PerformLayout();
            this.panel3.ResumeLayout(false);
            this.panel3.PerformLayout();
            this.panel5.ResumeLayout(false);
            this.panel5.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dataGridView1)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Panel panel1;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.TreeView treeView;
        private System.Windows.Forms.Panel panel2;
        private System.Windows.Forms.TextBox tbCurDepCode;
        private System.Windows.Forms.TextBox tbCurDepName;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Panel panel3;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.Panel panel5;
        private System.Windows.Forms.Button btnUpdateInfo;
        private System.Windows.Forms.CheckBox cbAutoUpdate;
        private System.Windows.Forms.NumericUpDown nudFreq;
        private System.Windows.Forms.Label lblFreq1;
        private System.Windows.Forms.Label lblFreq2;
        private System.Windows.Forms.ComboBox cbCurDepParentDep;
        private System.Windows.Forms.CheckBox cbLazyLoad;
        private System.Windows.Forms.DataGridView dataGridView1;
        private System.Windows.Forms.Button btnAddEmployee;
        private System.Windows.Forms.Button btnRemoveEmps;
        private System.Windows.Forms.Button btnUp;
        private System.Windows.Forms.Button btnLeft;
        private System.Windows.Forms.Button btnDown;
        private System.Windows.Forms.Button btnRight;
        private System.Windows.Forms.Button btnUndoDepartmentInfoChanges;
        private System.Windows.Forms.Button btnSaveDepartmentInfoChanges;
        private System.Windows.Forms.DataGridViewTextBoxColumn Column0Id;
        private System.Windows.Forms.DataGridViewTextBoxColumn Column1Name;
        private System.Windows.Forms.DataGridViewTextBoxColumn Column2Pos;
        private System.Windows.Forms.DataGridViewTextBoxColumn Column3Age;
        private System.Windows.Forms.DataGridViewButtonColumn Column4Edit;
        private System.Windows.Forms.DataGridViewButtonColumn Column5Remove;
        private System.Windows.Forms.DataGridViewCheckBoxColumn Column6CB;
        private System.Windows.Forms.Button btnRemoveCurDep;
        private System.Windows.Forms.Button btnAddNewDep;
        private System.Windows.Forms.CheckBox cbAskDel;
        private System.Windows.Forms.Label lblErrCode;
        private System.Windows.Forms.Label lblErrName;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.ComboBox cbActNotSave;
    }
}

